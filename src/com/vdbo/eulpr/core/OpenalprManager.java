package com.vdbo.eulpr.core;

import com.openalpr.jni.Alpr;
import com.openalpr.jni.AlprCoordinate;
import com.openalpr.jni.AlprPlateResult;
import com.openalpr.jni.AlprResults;
import com.vdbo.eulpr.model.RecognitionResult;
import com.vdbo.eulpr.utils.CropLicensePlate;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

public class OpenalprManager {

    private Alpr alpr;

    public OpenalprManager() {
        alpr = new Alpr("eu", "", "openalpr/runtime_data"); //"" = openalpr/openalpr.conf
    }

    public RecognitionResult getNumberPlate(String path, int numberOfResults, boolean isPlateImage, boolean isConfidence) {
        Image licencePlateImage = null;
        StringBuilder licensePlate = new StringBuilder();
        StringBuilder resultConfidence = new StringBuilder();
        int sizeOfResults;

        alpr.setTopN(numberOfResults);

        if (!alpr.recognize(path).getPlates().isEmpty()) {
            AlprPlateResult alprPlateResult = alpr.recognize(path).getPlates().get(0);
            sizeOfResults = alprPlateResult.getTopNPlates().size();

            for (int i = 0; i < sizeOfResults; i++) {
                licensePlate.append(alprPlateResult.getTopNPlates().get(i).getCharacters());
                licensePlate.append("\n");
                if (isConfidence) {
                    resultConfidence.append(alprPlateResult.getTopNPlates().get(i).getOverallConfidence());
                    resultConfidence.append("\n");
                }
            }
            if (isPlateImage) {
                ArrayList<AlprCoordinate> coordinates = (ArrayList<AlprCoordinate>) alprPlateResult.getPlatePoints();
                licencePlateImage = CropLicensePlate.fromImage(path, coordinates.get(0).getX(), coordinates.get(0).getY(), coordinates.get(1).getX(), coordinates.get(3).getY());
            }
        } else {
            licensePlate.append("can't recognize");
            licencePlateImage = new Image("file:" + path);
        }

        alpr.unload();

        return new RecognitionResult(path, licensePlate.toString(), resultConfidence.toString(), licencePlateImage);
    }

    public ArrayList<RecognitionResult> getNumberPlates(ArrayList<String> paths, int numberOfResults, boolean isPlateImage, boolean isConfidence) {
        ArrayList<RecognitionResult> recognitionResults = new ArrayList<>();
        AlprPlateResult alprPlateResult;
        Image licencePlateImage = null;
        StringBuilder licensePlate = new StringBuilder();
        StringBuilder resultConfidence = new StringBuilder();
        int sizeOfResults;

        alpr.setTopN(numberOfResults);

        for (String path : paths) {
            List<AlprPlateResult> alprPlateResultList = alpr.recognize(path).getPlates();
            licensePlate.delete(0, licensePlate.length());
            resultConfidence.delete(0, resultConfidence.length());
            if (!alprPlateResultList.isEmpty()) {
                alprPlateResult = alprPlateResultList.get(0);
                sizeOfResults = alprPlateResult.getTopNPlates().size();

                for (int i = 0; i < sizeOfResults; i++) {
                    licensePlate.append(alprPlateResult.getTopNPlates().get(i).getCharacters());
                    licensePlate.append("\n");
                    if (isConfidence) {
                        resultConfidence.append(alprPlateResult.getTopNPlates().get(i).getOverallConfidence());
                        resultConfidence.append("\n");
                    }
                }
                if (isPlateImage) {
                    ArrayList<AlprCoordinate> coordinates = (ArrayList<AlprCoordinate>) alprPlateResult.getPlatePoints();
                    licencePlateImage = CropLicensePlate.fromImage(path, coordinates.get(0).getX(), coordinates.get(0).getY(), coordinates.get(1).getX(), coordinates.get(3).getY());
                }
            } else {
                licensePlate.append("can't recognize image");
                licencePlateImage = new Image("file:" + path);
            }
            recognitionResults.add(new RecognitionResult(path, licensePlate.toString(), resultConfidence.toString(), licencePlateImage));
        }

        alpr.unload();

        return recognitionResults;
    }
}
