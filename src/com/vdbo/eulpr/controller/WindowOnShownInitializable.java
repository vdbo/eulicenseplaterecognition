package com.vdbo.eulpr.controller;

import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.stage.WindowEvent;

public interface WindowOnShownInitializable extends Initializable {

    EventHandler<WindowEvent> getOnWindowShown();

}
