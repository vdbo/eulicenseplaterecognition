package com.vdbo.eulpr.controller;

import com.vdbo.eulpr.ResultSettings;
import com.vdbo.eulpr.utils.AlertMessage;
import com.vdbo.eulpr.utils.ValidData;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    private CheckBox cbShowConfidence;
    @FXML
    private CheckBox cbShowPath;
    @FXML
    private CheckBox cbShowPlateImage;
    @FXML
    private GridPane rootPane;
    @FXML
    private TextField tfNumberOfResultsPlate;

    private ResultSettings resultSettings;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void onClickOpenImageBtn(ActionEvent actionEvent) throws IOException {
        openWindow("/com/vdbo/eulpr/res/ui/window_result.fxml", new ImageResultController(), "Recognition's result for one image", 1000, 700);
    }

    @FXML
    private void onClickReadPathsBtn(ActionEvent actionEvent) throws IOException {
        openWindow("/com/vdbo/eulpr/res/ui/window_result.fxml", new ImagesFromFileResultController(), "Recognition's result for images from file", 1000, 700);
    }

    @FXML
    private void onClickOpenImagesBtn(ActionEvent actionEvent) throws IOException {
        openWindow("/com/vdbo/eulpr/res/ui/window_result.fxml", new ImagesResultController(), "Recognition's result for multiple images", 1000, 700);
    }

    @FXML
    private void onClickSettingsMenuItem(ActionEvent actionEvent) throws IOException {
        openWindow("/com/vdbo/eulpr/res/ui/window_settings.fxml", new SettingsController(), "Settings", 600, 300);
    }

    @FXML
    private void onClickAboutMenuItem(ActionEvent actionEvent) throws IOException {
        openWindow("/com/vdbo/eulpr/res/ui/window_about.fxml", new AboutController(), "About", 550, 300);
    }

    private boolean setResultSettingsData() {
        resultSettings = new ResultSettings();

        resultSettings.setIsShowPlateImage(cbShowPlateImage.isSelected());
        resultSettings.setIsShowPathToImage(cbShowPath.isSelected());
        resultSettings.setIsShowConfidence(cbShowConfidence.isSelected());

        return areFieldsValid();
    }

    private boolean areFieldsValid() {
        boolean result = false;

        if (tfNumberOfResultsPlate.getText().isEmpty()) {
            AlertMessage.showErrorMessage("You have not input the number of results");
        } else if (!ValidData.isNumber(tfNumberOfResultsPlate.getText())) {
            AlertMessage.showErrorMessage("Please, input the NUMBER of results");
        } else if (Integer.parseInt(tfNumberOfResultsPlate.getText()) > 10) {
            AlertMessage.showInformationMessage("Number must be less than 10");
        } else {
            resultSettings.setNumberOfResultsPlate(Integer.parseInt(tfNumberOfResultsPlate.getText()));
            result = true;
        }

        return result;
    }

    private void openWindow(String pathToFxml, WindowOnShownInitializable controller, String windowTitle, int height, int width) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(pathToFxml));

        if (controller instanceof BaseResultController && !setResultSettingsData()) {
            return;
        } else if (controller instanceof BaseResultController) {
            ((BaseResultController) controller).setResultSettings(resultSettings);
        }
        loader.setController(controller);

        Stage stage = new Stage();
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(loader.load(), height, width));
        stage.setOnShowing(controller.getOnWindowShown());
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(rootPane.getScene().getWindow());
        stage.show();
    }
}
