package com.vdbo.eulpr.controller;

import com.vdbo.eulpr.core.OpenalprManager;
import com.vdbo.eulpr.file.FileReader;
import com.vdbo.eulpr.model.RecognitionResult;
import com.vdbo.eulpr.utils.FileDirectoryChooser;
import com.vdbo.eulpr.utils.Settings;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ImagesFromFileResultController extends BaseResultController {

    private RecognizeTask recognizeTask;

    private ArrayList<String> paths;

    @Override
    protected void initializeInstances() {
        onWindowShown = new OnWindowShown();
        recognizeTask =  new RecognizeTask();
        recognizeTask.setOnSucceeded(new OnSucceededRecognize());
        pbProgress.progressProperty().bind(recognizeTask.progressProperty());
    }

    private ArrayList<String> readPathsFromFile(String pathToFile) throws IOException{
        if (pathToFile == null || pathToFile.isEmpty()) {
            File file = FileDirectoryChooser.getInstance().openFile(primaryStage);
            if (file != null) {
                return FileReader.readText(file.getPath());
            } else {
                return new ArrayList<>();
            }
        }
        return FileReader.readText(pathToFile);
    }

    private class OnWindowShown implements EventHandler<WindowEvent> {

        @Override
        public void handle(WindowEvent event) {
            try {
                paths = readPathsFromFile(Settings.getInstance().getDefaultReadFilePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            new Thread(recognizeTask).start();
        }
    }

    private class RecognizeTask extends Task<Void> {

        @Override
        protected Void call() throws Exception {
            if (paths != null ) {
                OpenalprManager openalprManager = new OpenalprManager();

                recognitionResultOfPlates = openalprManager.getNumberPlates(paths, resultSettings.getNumberOfResultsPlate(), resultSettings.isShowPlateImage(), resultSettings.isShowConfidence());
                for (RecognitionResult recognitionOfPlate : recognitionResultOfPlates) {
                    platesForTableView.add(recognitionOfPlate);
                }
            }
            return null;
        }
    }

}
