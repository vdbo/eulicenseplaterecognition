package com.vdbo.eulpr.controller;

import com.vdbo.eulpr.ResultSettings;
import com.vdbo.eulpr.file.FileWriter;
import com.vdbo.eulpr.model.RecognitionResult;
import com.vdbo.eulpr.utils.FileDirectoryChooser;
import com.vdbo.eulpr.utils.Settings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.control.Button;
import javafx.util.Callback;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public abstract class BaseResultController implements WindowOnShownInitializable {

    @FXML
    protected GridPane rootPane;
    @FXML
    protected Stage primaryStage;
    @FXML
    protected Button btnSaveToFile;
    @FXML
    protected TableView<RecognitionResult> tvResult;
    @FXML
    protected ProgressBar pbProgress;

    protected FileDirectoryChooser fileDirectoryChooser;
    protected ResultSettings resultSettings;
    protected EventHandler<WindowEvent> onWindowShown;

    protected ArrayList<RecognitionResult> recognitionResultOfPlates;
    protected ObservableList<RecognitionResult> platesForTableView;

    public void setResultSettings(ResultSettings resultSettings) {
        this.resultSettings = resultSettings;
    }

    public EventHandler<WindowEvent> getOnWindowShown() {
        return onWindowShown;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fileDirectoryChooser = FileDirectoryChooser.getInstance();
        platesForTableView = FXCollections.observableArrayList();

        initializeInstances();
    }

    protected void initializeTVColumns() {
        if (resultSettings.isShowPlateImage()) {
            initializeColumn("License plate's image", "licensePlateImage", new ImageColumnCellFactory());
        }
        if (resultSettings.isShowPathToImage()) {
            initializeColumn("Image's path", "pathImage", null);
        }
        initializeColumn("Plate's number", "plateNumber", null);
        if (resultSettings.isShowConfidence()) {
            initializeColumn("Confidence", "confidence", null);
        }
    }

    private <T> void initializeColumn(String columnTitle, String fieldName, Callback<TableColumn<RecognitionResult, T>, TableCell<RecognitionResult, T>> cellFactory) {
        TableColumn<RecognitionResult, T> column = new TableColumn<>(columnTitle);
        column.setCellValueFactory(new PropertyValueFactory<>(fieldName));
        if (cellFactory != null) {
            column.setCellFactory(cellFactory);
        }
        tvResult.getColumns().add(column);
    }

    @FXML
    protected void onClickSaveToFileBtn(ActionEvent actionEvent) throws IOException {
        String path = Settings.getInstance().getDefaultWriteFilePath();
        if (path == null || path.isEmpty()) {
            File file = fileDirectoryChooser.openFile(primaryStage);
            if (file != null) {
                FileWriter.writeText(file.getPath(), recognitionResultOfPlates);
            }
            return;
        }
        FileWriter.writeText(path, recognitionResultOfPlates);
    }

    protected abstract void initializeInstances();

    private class ImageColumnCellFactory implements Callback<TableColumn<RecognitionResult, Image>, TableCell<RecognitionResult, Image>> {

        @Override
        public TableCell<RecognitionResult, Image> call(TableColumn<RecognitionResult, Image> param) {
            return new TableCell<RecognitionResult, Image>() {
                @Override
                protected void updateItem(Image item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item != null) {
                        setText(null);
                        ImageView imageView = new ImageView(item);
                        if (item.getRequestedHeight() == 0.0 || item.getRequestedWidth() == 0.0) {
                            imageView.setFitHeight(250);
                            imageView.setFitWidth(271);
                        } else {
                            imageView.setFitHeight(45);
                            imageView.setFitWidth(210);
                        }
                        setGraphic(imageView);
                    } else {
                        setGraphic(null);
                    }
                }
            };
        }
    }

    protected class OnSucceededRecognize implements EventHandler<WorkerStateEvent> {

        @Override
        public void handle(WorkerStateEvent event) {
            initializeTVColumns();
            tvResult.setItems(platesForTableView);
            pbProgress.setVisible(false);
            btnSaveToFile.setVisible(true);
        }
    }
}
