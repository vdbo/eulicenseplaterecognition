package com.vdbo.eulpr.controller;

import com.vdbo.eulpr.core.OpenalprManager;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import com.vdbo.eulpr.model.RecognitionResult;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


import java.io.File;
import java.util.ArrayList;

public class ImageResultController extends BaseResultController {

    private RecognizeTask recognizeTask;

    private File file;

    @Override
    protected void initializeInstances() {
        onWindowShown = new OnWindowShown();
        recognitionResultOfPlates = new ArrayList<>(1);
        recognizeTask = new RecognizeTask();
        recognizeTask.setOnSucceeded(new OnSucceededRecognize());
        pbProgress.progressProperty().bind(recognizeTask.progressProperty());
    }

    private class OnWindowShown implements EventHandler<WindowEvent> {

        @Override
        public void handle(WindowEvent event) {
            primaryStage = (Stage) rootPane.getScene().getWindow();
            file = fileDirectoryChooser.openFile(primaryStage);

            new Thread(recognizeTask).start();
        }
    }

    private class RecognizeTask extends Task<Void> {

        @Override
        protected Void call() throws Exception {
            recognitionResultOfPlates.clear();

            if (file != null) {
                OpenalprManager openalprManager = new OpenalprManager();

                RecognitionResult recognitionOfPlate = openalprManager.getNumberPlate(file.getPath(), resultSettings.getNumberOfResultsPlate(), resultSettings.isShowPlateImage(), resultSettings.isShowConfidence());
                recognitionResultOfPlates.add(recognitionOfPlate);

                platesForTableView.add(recognitionOfPlate);
            }
            return null;
        }
    }

}