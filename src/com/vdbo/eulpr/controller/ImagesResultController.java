package com.vdbo.eulpr.controller;

import com.vdbo.eulpr.core.OpenalprManager;
import com.vdbo.eulpr.model.RecognitionResult;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImagesResultController extends BaseResultController {

    private RecognizeTask recognizeTask;

    private List<File> files;

    @Override
    protected void initializeInstances() {
        onWindowShown = new OnWindowShown();
        recognizeTask = new RecognizeTask();
        recognizeTask.setOnSucceeded(new OnSucceededRecognize());
        pbProgress.progressProperty().bind(recognizeTask.progressProperty());
    }

    private class OnWindowShown implements EventHandler<WindowEvent> {

        @Override
        public void handle(WindowEvent event) {
            primaryStage = (Stage) rootPane.getScene().getWindow();
            files = fileDirectoryChooser.openFiles(primaryStage);

            new Thread(recognizeTask).start();
        }
    }

    private class RecognizeTask extends Task<Void> {

        @Override
        protected Void call() throws Exception {
            if (files != null ) {
                OpenalprManager openalprManager = new OpenalprManager();

                ArrayList<String> paths = new ArrayList<>();
                for (File file : files) {
                    paths.add(file.getPath());
                }
                recognitionResultOfPlates = openalprManager.getNumberPlates(paths, resultSettings.getNumberOfResultsPlate(), resultSettings.isShowPlateImage(), resultSettings.isShowConfidence());
                for (RecognitionResult recognitionsOfPlate : recognitionResultOfPlates) {
                    platesForTableView.add(recognitionsOfPlate);
                }
            }
            return null;
        }
    }
}
