package com.vdbo.eulpr.controller;

import com.vdbo.eulpr.utils.FileDirectoryChooser;
import com.vdbo.eulpr.utils.Settings;
import com.vdbo.eulpr.utils.ValidData;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements WindowOnShownInitializable {

    @FXML
    private Label lblDirectoryPath;
    @FXML
    private Label lblFileWritePath;
    @FXML
    private Label lblFileReadPath;
    @FXML
    private VBox rootPane;
    @FXML
    private Stage primaryStage;

    private EventHandler<WindowEvent> onWindowShown;
    private FileDirectoryChooser fileDirectoryChooser;
    private Settings settings;

    public EventHandler<WindowEvent> getOnWindowShown() {
        return onWindowShown;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fileDirectoryChooser = FileDirectoryChooser.getInstance();
        onWindowShown = new OnWindowShown();
        settings = Settings.getInstance();
    }

    @FXML
    private void onClickBrowseDirectory(ActionEvent actionEvent) {
        File file = fileDirectoryChooser.openDirectory(primaryStage);
        if (file != null) {
            if (ValidData.isPathValid(file.getPath())) {
                lblDirectoryPath.setText(file.getPath());
            }
        }
    }

    @FXML
    private void onClickBrowseWriteFile(ActionEvent actionEvent) {
        File file = fileDirectoryChooser.openFile(primaryStage);
        if (file != null) {
            if (ValidData.isFileEditable(file.getPath())) {
                lblFileWritePath.setText(file.getPath());
            }
        }
    }

    @FXML
    private void onClickBrowseReadFile(ActionEvent actionEvent) {
        File file = fileDirectoryChooser.openFile(primaryStage);
        if (file != null) {
            if (ValidData.isFileEditable(file.getPath())) {
                lblFileReadPath.setText(file.getPath());
            }
        }
    }

    @FXML
    private void onClickSaveChanges(ActionEvent actionEvent) {
        if (ValidData.isPathValid(lblDirectoryPath.getText())) {
            settings.setDefaultDirectoryPath(lblDirectoryPath.getText());
        }
        if (ValidData.isPathValid(lblFileWritePath.getText())) {
            settings.setDefaultWriteFilePath(lblFileWritePath.getText());
        }
        if (ValidData.isPathValid(lblFileReadPath.getText())) {
            settings.setDefaultReadFilePath(lblFileReadPath.getText());
        }
        primaryStage.close();
    }

    private class OnWindowShown implements EventHandler<WindowEvent> {

        @Override
        public void handle(WindowEvent event) {
            primaryStage = (Stage) rootPane.getScene().getWindow();
            if (settings.getDefaultDirectoryPath() == null || settings.getDefaultDirectoryPath().isEmpty()) {
                lblDirectoryPath.setText("Default directory path");
            } else {
                lblDirectoryPath.setText(settings.getDefaultDirectoryPath());
            }
            if (settings.getDefaultWriteFilePath() == null || settings.getDefaultWriteFilePath().isEmpty()) {
                lblFileWritePath.setText("Default file path for writing");
            } else {
                lblFileWritePath.setText(settings.getDefaultWriteFilePath());
            }
            if (settings.getDefaultReadFilePath() == null || settings.getDefaultReadFilePath().isEmpty()) {
                lblFileReadPath.setText("Default file path for reading");
            } else {
                lblFileReadPath.setText(settings.getDefaultReadFilePath());
            }
        }
    }
}
