package com.vdbo.eulpr.utils;

import java.io.IOException;
import java.io.Serializable;

public class Settings implements Serializable {

    private static Settings settings;

    private String defaultDirectoryPath;
    private String defaultWriteFilePath;
    private String defaultReadFilePath;

    public static Settings getInstance() {
        if (settings == null) {
            settings = new Settings();
            try {
                settings = Serialization.deserialize();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return settings;
    }

    public String getDefaultWriteFilePath() {
        return defaultWriteFilePath;
    }

    public void setDefaultWriteFilePath(String defaultWriteFilePath) {
        this.defaultWriteFilePath = defaultWriteFilePath;
    }

    public String getDefaultReadFilePath() {
        return defaultReadFilePath;
    }

    public void setDefaultReadFilePath(String defaultReadFilePath) {
        this.defaultReadFilePath = defaultReadFilePath;
    }

    public String getDefaultDirectoryPath() {
        return defaultDirectoryPath;
    }

    public void setDefaultDirectoryPath(String defaultDirectoryPath) {
        this.defaultDirectoryPath = defaultDirectoryPath;
    }

}
