package com.vdbo.eulpr.utils;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;

public class CropLicensePlate {

    public static Image fromImage(String path, int leftTopX, int leftTopY, int rightTopX, int leftBottomY) {
        Image image = new Image("file:" + path);
        PixelReader reader = image.getPixelReader();
        int width = rightTopX - leftTopX;
        int height = leftBottomY - leftTopY;
        WritableImage newImage = new WritableImage(reader, leftTopX, leftTopY, width, height);
        return newImage;
    }
}
