package com.vdbo.eulpr.utils;

import java.util.regex.Pattern;

public class ValidData {

    private static final String FILE_PATH = "^/[/a-zA-Zа-яА-Я0-9\\p{Punct}\\s]+$";
    private static final String NUMBER = "^\\d+$";
    private static final String EDITABLE = "^/[/a-zA-Zа-яА-Я0-9\\p{Punct}\\s]+[txt]$";

    public static boolean isPathValid(String s) {
        return Pattern.compile(FILE_PATH).matcher(s).matches();
    }

    public static boolean isNumber(String s) {
        return Pattern.compile(NUMBER).matcher(s).matches();
    }

    public static boolean isFileEditable(String s) {
        return Pattern.compile(EDITABLE).matcher(s).matches();
    }

}
