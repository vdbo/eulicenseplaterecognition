package com.vdbo.eulpr.utils;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.List;
import java.util.Set;

public class FileDirectoryChooser {

    private static FileDirectoryChooser fileDirectoryChooser;

    private FileChooser fileChooser;
    private DirectoryChooser directoryChooser;

    private void setFileChooser(FileChooser fileChooser) {
        this.fileChooser = fileChooser;
    }

    private void setDirectoryChooser(DirectoryChooser directoryChooser) {
        this.directoryChooser = directoryChooser;
    }

    public static FileDirectoryChooser getInstance() {
        if (fileDirectoryChooser == null) {
            fileDirectoryChooser = new FileDirectoryChooser();
            fileDirectoryChooser.setFileChooser(new FileChooser());
            fileDirectoryChooser.setDirectoryChooser(new DirectoryChooser());
        }
        return fileDirectoryChooser;
    }

    private void setInitialDirectory() {
        if (Settings.getInstance().getDefaultDirectoryPath() != null) {
            File file = new File(Settings.getInstance().getDefaultDirectoryPath());
            fileChooser.setInitialDirectory(file);
        }
    }

    public File openFile(Stage stage) {
        setInitialDirectory();
        return fileChooser.showOpenDialog(stage);
    }

    public List<File> openFiles(Stage stage) {
        setInitialDirectory();
        return fileChooser.showOpenMultipleDialog(stage);
    }

    public File openDirectory(Stage stage) {
        setInitialDirectory();
        return directoryChooser.showDialog(stage);
    }

}
