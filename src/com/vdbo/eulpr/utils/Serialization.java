package com.vdbo.eulpr.utils;

import java.io.*;

public class Serialization {

    private static final String SERIALIZE_FILE_NAME = "settings.dat";

    public static void serialize(Settings object) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(SERIALIZE_FILE_NAME));
        objectOutputStream.writeObject(object);
        objectOutputStream.close();
    }

    public static Settings deserialize() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(SERIALIZE_FILE_NAME));
        Object object = objectInputStream.readObject();
        objectInputStream.close();
        return (Settings) object;
    }

}
