package com.vdbo.eulpr.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;

public class RecognitionResult {

    private SimpleStringProperty pathImage;
    private SimpleStringProperty plateNumber;
    private SimpleStringProperty confidence;

    private ObjectProperty<Image> licensePlateImage;

    public RecognitionResult(String pathImage, String plateNumber, String confidence, Image licensePlateImage) {
        this.pathImage = new SimpleStringProperty(pathImage);
        this.plateNumber = new SimpleStringProperty(plateNumber);
        this.confidence = new SimpleStringProperty(confidence);
        this.licensePlateImage = new SimpleObjectProperty<>(licensePlateImage);
    }

    public String getPathImage() {
        return pathImage.get();
    }

    public void setPathImage(String pathImage) {
        this.pathImage.set(pathImage);
    }

    public String getPlateNumber() {
        return plateNumber.get();
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber.set(plateNumber);
    }

    public String getConfidence() {
        return confidence.get();
    }

    public void setConfidence(String confidence) {
        this.confidence.set(confidence);
    }

    public Image getLicensePlateImage() {
        return licensePlateImage.get();
    }

    public void setLicensePlateImage(Image licensePlateImage) {
        this.licensePlateImage.set(licensePlateImage);
    }
}
