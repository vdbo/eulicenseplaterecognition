package com.vdbo.eulpr;

import com.openalpr.jni.Alpr;
import com.openalpr.jni.AlprPlateResult;
import com.openalpr.jni.AlprResults;

import com.vdbo.eulpr.utils.Serialization;
import com.vdbo.eulpr.utils.Settings;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class MainWindow extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("res/ui/window_main.fxml"));
        primaryStage.setTitle("European Union's license plate recognition");
        primaryStage.setScene(new Scene(root, 700, 600));
        primaryStage.setOnCloseRequest(new OnCloseRequestListener());
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private class OnCloseRequestListener implements EventHandler<WindowEvent> {

        @Override
        public void handle(WindowEvent event) {
            try {
                Serialization.serialize(Settings.getInstance());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
