package com.vdbo.eulpr.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileReader {

    private static final String TEXT_CODING = "UTF-8";

    public static ArrayList<String> readText(String path) throws IOException {
        ArrayList<String> strings = new ArrayList<>();

        Path file = Paths.get(path);
        BufferedReader reader = Files.newBufferedReader(file, Charset.forName(TEXT_CODING));
        String line;

        while ((line = reader.readLine()) != null) {
            strings.add(line);
        }

        reader.close();
        return strings;
    }

}
