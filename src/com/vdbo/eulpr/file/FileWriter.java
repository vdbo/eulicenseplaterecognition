package com.vdbo.eulpr.file;

import com.vdbo.eulpr.model.RecognitionResult;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileWriter {

    private static final String TEXT_CODING = "UTF-8";

    public static void writeText(String path, ArrayList<RecognitionResult> results) throws IOException {
        Path file = Paths.get(path);
        BufferedWriter writer = Files.newBufferedWriter(file, Charset.forName(TEXT_CODING));
        for (RecognitionResult text : results) {
            writer.write(text.getPathImage() + "\n");
            writer.write(text.getPlateNumber() + "\n");
            writer.write(text.getConfidence() + "\n");
            writer.write("\n");
        }
        writer.close();
    }

}
