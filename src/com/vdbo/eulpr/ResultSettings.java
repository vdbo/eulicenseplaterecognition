package com.vdbo.eulpr;

public class ResultSettings {

    private boolean isShowPlateImage;
    private boolean isShowPathToImage;
    private boolean isShowConfidence;
    private int numberOfResultsPlate;

    public boolean isShowPlateImage() {
        return isShowPlateImage;
    }

    public void setIsShowPlateImage(boolean isShowPlateImage) {
        this.isShowPlateImage = isShowPlateImage;
    }

    public boolean isShowPathToImage() {
        return isShowPathToImage;
    }

    public void setIsShowPathToImage(boolean isShowPathToImage) {
        this.isShowPathToImage = isShowPathToImage;
    }

    public boolean isShowConfidence() {
        return isShowConfidence;
    }

    public void setIsShowConfidence(boolean isShowConfidence) {
        this.isShowConfidence = isShowConfidence;
    }

    public int getNumberOfResultsPlate() {
        return numberOfResultsPlate;
    }

    public void setNumberOfResultsPlate(int numberOfResultsPlate) {
        this.numberOfResultsPlate = numberOfResultsPlate;
    }
}
